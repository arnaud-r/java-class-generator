import javax.swing.JPanel;
import java.awt.Color;
import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.Insets;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import javax.swing.JButton;
import javax.swing.JCheckBox;
import javax.swing.JComboBox;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JTextField;

public class PanelFils extends JPanel implements ActionListener
{	
	JLabel labClassType = new JLabel("Type:");
	JComboBox combClassType = new JComboBox(Constante.szClassState);
	
	JLabel labClassName = new JLabel("Name:");
	JTextField szBoxClassName = new JTextField(10);
	
	JLabel labClassHerit = new JLabel("Inheritance:");
	JTextField szBoxClassHerit = new JTextField(10);
	
	JLabel labClassInter = new JLabel("Interface(s):");
	JTextField szBoxClassInter = new JTextField(10);
	
	static JButton butFuncs = new JButton("Add functions");
	
	JButton butPreview = new JButton("Preview");
	JButton butGenerate = new JButton("Generate");
	JButton butReset = new JButton("Reset");
	
	JOptionPane msgBox = new JOptionPane();
	
	JLabel labCredit = new JLabel("Made by Nitrix");
	
	//stock la fenetre de creation de methodes
	static FenetreMere curWin;
	//stock le nombre de fonction a cr�er
	static int iNbFuncSave = 0;
	
	public PanelFils()
	{
		setLayout(new GridBagLayout());	
		GridBagConstraints cons = new GridBagConstraints();
		
		cons.anchor = GridBagConstraints.WEST;
		cons.insets = new Insets(5, 5, 5, 5);
				
		//1ere ligne (classtype stuff)
		cons.gridx = 0;
		cons.gridy = 0;
		cons.gridwidth = 1;
		cons.gridheight = 1;
		add(labClassType, cons);
		
		cons.gridx = 1;
		cons.gridwidth = 2;
		add(combClassType, cons);
		
		//2eme ligne (classname stuff)
		cons.gridx = 0;
		cons.gridy = 1;
		cons.gridwidth = 1;
		cons.gridheight = 1;
		add(labClassName, cons);
				
		cons.gridx = 1;
		cons.gridwidth = 2;
		add(szBoxClassName, cons);
		
		//3eme ligne (heritage stuff)
		cons.gridx = 0;
		cons.gridy = 2;
		cons.gridwidth = 1;
		cons.gridheight = 1;
		add(labClassHerit, cons);
						
		cons.gridx = 1;
		cons.gridwidth = 2;
		add(szBoxClassHerit, cons);
		
		//4eme ligne (interface stuff)
		cons.gridx = 0;
		cons.gridy = 3;
		cons.gridwidth = 1;
		cons.gridheight = 1;
		add(labClassInter, cons);
								
		cons.gridx = 1;
		cons.gridwidth = 2;
		add(szBoxClassInter, cons);
		
		//5eme ligne (bouton ajout methodes)
		cons.anchor = GridBagConstraints.CENTER;
		cons.gridx = 0;
		cons.gridy = 4;
		cons.gridwidth = 4;
		cons.gridheight = 1;
		butFuncs.addActionListener(this);
		add(butFuncs, cons);
		
		//6eme ligne (bouton preview)
		cons.gridx = 0;
		cons.gridy = 5;
		cons.gridwidth = 3;
		cons.gridheight = 1;
		butPreview.addActionListener(this);
		add(butPreview, cons);
		
		//7eme ligne (bouton generate)
		cons.gridx = 0;
		cons.gridy = 6;
		cons.gridwidth = 3;
		cons.gridheight = 1;
		butGenerate.setBackground(new Color(0,255,0));
		butGenerate.setForeground(new Color(255, 255, 255));
		butGenerate.addActionListener(this);
		add(butGenerate, cons);
		
		//8e ligne (bouton reset)
		cons.gridx = 0;
		cons.gridy = 7;
		cons.gridwidth = 3;
		cons.gridheight = 1;
		butReset.setBackground(new Color(255, 0, 0));
		butReset.setForeground(new Color(255, 255, 255));
		butReset.addActionListener(this);
		add(butReset, cons);
		
		//9e ligne (credits)
		cons.gridx = 0;
		cons.gridy = 8;
		cons.gridwidth = 3;
		cons.gridheight = 1;
		add(labCredit, cons);
	}


	public void actionPerformed(ActionEvent arg0) 
	{
		if((JButton)arg0.getSource() == butGenerate || (JButton)arg0.getSource() == butPreview)
		{
			//si la classe n'a pas de nom, on quitte
			if(szBoxClassName.getText().isEmpty())
			{
				msgBox.showMessageDialog(null, "Please, give a name to your class", "Error", JOptionPane.ERROR_MESSAGE);
				return;
			}
				
			//differents cas possible de fonctions (sans implements, avec, sans ...)
			String stuff = new String();
			if(szBoxClassInter.getText().isEmpty() && szBoxClassHerit.getText().isEmpty())
				stuff += "\n";
			else if(szBoxClassInter.getText().isEmpty())
				stuff += " extends " + szBoxClassHerit.getText() + "\n";		
			else if(szBoxClassHerit.getText().isEmpty())
				stuff += " implements " + szBoxClassInter.getText() + "\n";			
			else
				stuff += " extends " + szBoxClassHerit.getText() + " implements " + szBoxClassInter.getText() + "\n";		
				
			//full texte classe + functions
			String szText = new String();		
			szText = ((String)combClassType.getSelectedItem() + " class " + szBoxClassName.getText() + stuff
							+ "{ \n"
							+ (PanelFunction.funcStr.isEmpty() ? "\n \n \n \n" : PanelFunction.funcStr)
							+ "}");
			
			if((JButton)arg0.getSource() == butPreview)
			{
				new FenetreMere(new PanelPreview(szText), false);
			}		
			else if((JButton)arg0.getSource() == butGenerate)
			{
				//Creation du fichier .java
				Fichier curClass = new Fichier(szBoxClassName.getText());
				if(curClass != null)
				{
					//on ajoute le text dans le fichier
					curClass.addText(szText);
						
					//on oublie pas de close le file a la fin
					curClass.closeFile();
								
					//success message
					msgBox.showMessageDialog(null, "Done!", "JAVA Class Gen by Nitrix", JOptionPane.INFORMATION_MESSAGE);
				}
			}
		}
		else if((JButton)arg0.getSource() == butReset)
		{
			//on reset tout a null
			combClassType.setSelectedIndex(0);
			szBoxClassName.setText(null);
			szBoxClassHerit.setText(null);
			szBoxClassInter.setText(null);
			PanelFunction.funcStr = "";
		}
		else if((JButton)arg0.getSource() == butFuncs)
		{
			//si la fenetre n'existe pas, on en cr�e une
			if(curWin == null)
				curWin = new FenetreMere(new PanelFunction(), false);
			//sinon, on d�truit la current, et on en cr�e une nouvelle
			else
			{
				curWin.dispose();
				curWin = new FenetreMere(new PanelFunction(), false);
			}
		}
	}
}
