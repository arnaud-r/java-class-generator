import javax.swing.JPanel;
import javax.swing.JTextArea;
import java.awt.Insets;

public class PanelPreview extends JPanel
{
	JTextArea szTextPreview = new JTextArea();
	
	public PanelPreview(String szText)
	{	
		//on affiche notre texte
		szTextPreview.setText(szText);
		//on applique a la fenetre la taille n�cessaire au texte
		szTextPreview.setSize(szTextPreview.getPreferredSize());
		//on laisse une petite marge entre le texte et la fenetre
		szTextPreview.setMargin(new Insets(10, 10, 10, 10));
		//on d�sactive l'�dition dans la fenetre
		szTextPreview.setEditable(false);
		//on add au panel
		add(szTextPreview);	
	}
}
