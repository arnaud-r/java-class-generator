import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.Insets;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import javax.swing.JButton;
import javax.swing.JComboBox;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JTextField;

public class PanelFunction extends JPanel implements ActionListener
{	
	public static String funcStr = new String();
	public int iNbFunctions = PanelFils.iNbFuncSave;
		
	JComboBox combNbFunc;
	
	JLabel labTitle[] = new JLabel[4];
	JLabel labFunc[] = new JLabel[50];
	JComboBox combFuncState[] = new JComboBox[50];
	JComboBox combFuncType[] = new JComboBox[50];
	JTextField szBoxFuncName[] = new JTextField[50];
	JTextField szBoxFuncArgs[] = new JTextField[50];
	
	String strNombres[] = new String[50];
	
	JButton butValider = new JButton("Save");
	
	JOptionPane msgBox = new JOptionPane();
	
	public PanelFunction()
	{
		setLayout(new GridBagLayout());	
		GridBagConstraints cons = new GridBagConstraints();
		
		cons.anchor = GridBagConstraints.WEST;
		cons.insets = new Insets(5, 5, 5, 5);
		
		//on init notre tableau de nombre
		for(int i = 0; i < 50; i++)
			strNombres[i] = Integer.toString(i);
		
		combNbFunc = new JComboBox(strNombres);
		
		cons.anchor = GridBagConstraints.WEST;
		cons.insets = new Insets(5, 5, 5, 5);
		
		//1ere ligne (nb de fonctions)
		cons.gridx = 0;
		cons.gridy = 0;
		cons.gridwidth = 1;
		cons.gridheight = 1;
		combNbFunc.setSelectedIndex(iNbFunctions);
		combNbFunc.addActionListener(this);
		add(combNbFunc, cons);
		
		if(iNbFunctions == 0)
			return;
		
		//titre tes colonnes
		for(int i = 0; i < 4; i++)
		{
			labTitle[i] = new JLabel(Constante.szFuncTabTitle[i]);
			cons.gridx = i > 1 ? i+3 : i+1;
			cons.gridy = 0;
			cons.gridwidth = i > 1 ? 3 : 1;
			cons.gridheight = 1;	
			add(labTitle[i], cons);
		}
						
		for(int i = 0; i < iNbFunctions; i++)
		{
			//label
			labFunc[i] = new JLabel("Function" + (i+1) + ":");
			cons.gridx = 0;
			cons.gridy = i+1;
			cons.gridwidth = 1;
			cons.gridheight = 1;
			add(labFunc[i], cons);
			
			//combobox public, private...
			combFuncState[i] = new JComboBox(Constante.szClassState);
			cons.gridx = 1;
			cons.gridy = i+1;
			cons.gridwidth = 1;
			cons.gridheight = 1;
			add(combFuncState[i], cons);
			
			//combobox type
			combFuncType[i] = new JComboBox(Constante.szFuncType);
			cons.gridx = 2;
			cons.gridy = i+1;
			cons.gridwidth = 2;
			cons.gridheight = 1;
			combFuncType[i].setEditable(true);
			add(combFuncType[i], cons);
			
			//textbox nom d'la func
			szBoxFuncName[i] = new JTextField(10);
			cons.gridx = 4;
			cons.gridy = i+1;
			cons.gridwidth = 2;
			cons.gridheight = 1;
			add(szBoxFuncName[i], cons);
			
			//textbox args d'la func
			szBoxFuncArgs[i] = new JTextField(10);
			cons.gridx = 6;
			cons.gridy = i+1;
			cons.gridwidth = 2;
			cons.gridheight = 1;
			add(szBoxFuncArgs[i], cons);
			
			// on place le bouton valider a la fin
			if(i == (iNbFunctions - 1))
			{
				cons.gridx = 0;
				cons.gridy = i+2;
				cons.gridwidth = 2;
				cons.gridheight = 1;
				butValider.addActionListener(this);
				add(butValider, cons);
			}
		}
	}

	public void actionPerformed(ActionEvent e) 
	{
		if(e.getSource() == combNbFunc)
		{
			//on actu la frame
			PanelFils.iNbFuncSave = Integer.parseInt((String)combNbFunc.getSelectedItem());
			PanelFils.butFuncs.doClick();
		}
		else if(e.getSource() == butValider)
		{
			//on cherche des fonctions sans nom
			for(int i = 0; i < iNbFunctions; i++)
			{
				if(szBoxFuncName[i].getText().isEmpty())
				{
					msgBox.showMessageDialog(null, "Please, give a name to your functions", "Error", JOptionPane.ERROR_MESSAGE);			
					return;
				}		
			}
			
			//ok cest valide on stock nos funcs dans un string
			for(int i = 0; i < iNbFunctions; i++)
			{
				funcStr += "\t" + (String)combFuncState[i].getSelectedItem() + ((String)combFuncType[i].getSelectedItem() == "none" ? "" : " " + (String)combFuncType[i].getSelectedItem()) + " " + szBoxFuncName[i].getText() + "(" + szBoxFuncArgs[i].getText() + ")\n";
				funcStr += "\t{ \n";
				funcStr += "\t\t\n \n";
				funcStr += "\t} \n";
			}
			
			//on ferme la fenetre
			PanelFils.curWin.dispose();
		}
	}
}
