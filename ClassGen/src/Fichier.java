import java.io.File;
import java.io.FileNotFoundException;
import java.io.PrintWriter;

public class Fichier 
{
	private PrintWriter fichier;
	
	public Fichier(String fileName)
	{
		try
		{
			fichier = new PrintWriter(fileName + ".java");
		}
		catch(FileNotFoundException ex)
		{
			System.out.println(ex.getMessage());
		}
	}
	
	public void addText(String arg) { fichier.println(arg);	 }
	
	public void deleteAllText() { fichier.flush(); }
	
	public void closeFile() { fichier.close(); }
}
