import javax.swing.JFrame;
import javax.swing.JPanel;

/*
  ____           _   _ _ _        _      
 |  _ \         | \ | (_) |      (_)     
 | |_) |_   _   |  \| |_| |_ _ __ ___  __
 |  _ <| | | |  | . ` | | __| '__| \ \/ /
 | |_) | |_| |  | |\  | | |_| |  | |>  < 
 |____/ \__, |  |_| \_|_|\__|_|  |_/_/\_\
         __/ |                           
        |___/                            
 */

public class FenetreMere extends JFrame 
{	
	public FenetreMere(JPanel panel, boolean bCloseall)
	{   
		super("JAVA Class Gen by Nitrix"); 
		setContentPane(panel);
		//on ferme toutes les fenetre ouvertes lorsqu'on clic sur la croix
		if(bCloseall)
			setDefaultCloseOperation(EXIT_ON_CLOSE);
		//auto-resize
		pack();
		//bloque le resize qui casse tout
		setMinimumSize(getPreferredSize());
		setVisible(true);
		setLocation(800, 250);
		
	}
	 	
	public static void main(String args[]) 
	{
		new FenetreMere(new PanelFils(), true);
	}
	
}